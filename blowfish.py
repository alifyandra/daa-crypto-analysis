from memory_profiler import profile
from Crypto.Random import get_random_bytes
from Crypto.Cipher import Blowfish
import cProfile, os, sys

key = get_random_bytes(16)
iv = get_random_bytes(8)

def blowfish_encrypt(message):
    cipher = Blowfish.new(key, Blowfish.MODE_CFB, iv)
    encryption = cipher.encrypt(message)
    return encryption

def blowfish_decrypt(ciphertext):
    cipher = Blowfish.new(key, Blowfish.MODE_CFB, iv)
    decryption = cipher.decrypt(ciphertext)
    return decryption

@profile
def encrypt_profile(message):
    return blowfish_encrypt(message)

@profile
def decrypt_profile(message):
    return blowfish_decrypt(message)

def main():
    size = sys.argv[1]
    message = open(os.path.join('textfiles',size+'.txt'),'r').read().encode('utf-8')

    print(f'{size}b Blowfish Encrypt Memory Data:')
    encrypted = encrypt_profile(message)

    print(f'\n\n{size}b Blowfish Decrypt Memory Data:')
    decrypt_profile(encrypted)

    print(f'\n\n{size}b Blowfish Encryption cProfile Data:')
    cProfile.runctx('blowfish_encrypt(message)',{'blowfish_encrypt':blowfish_encrypt,'message':message},{})

    print(f'\n\n{size}b Blowfish Decryption cProfile Data:')
    cProfile.runctx('blowfish_decrypt(message)',{'blowfish_decrypt':blowfish_decrypt,'message':encrypted},{})

if __name__ == "__main__":
    main()