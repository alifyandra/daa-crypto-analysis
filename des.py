from memory_profiler import profile
from Crypto.Cipher import DES
from Crypto.Random import get_random_bytes
import cProfile, os, sys
key = get_random_bytes(8)
IV = get_random_bytes(8)

def des_encrypt(message):
    mode = DES.MODE_CFB
    encryptor = DES.new(key, mode, IV=IV)
    ciphertext = encryptor.encrypt(message)
    return ciphertext

def des_decrypt(ciphertext):
    mode = DES.MODE_CFB
    decryptor = DES.new(key, mode, IV=IV)
    plaintext = decryptor.decrypt(ciphertext)
    return plaintext

@profile
def encrypt_profile(message):
    return des_encrypt(message)

@profile
def decrypt_profile(message):
    return des_decrypt(message)

def main():
    size = sys.argv[1]
    message = open(os.path.join('textfiles',size+'.txt'),'r').read().encode('utf-8')

    print(f'{size}b DES Encrypt Memory Data:')
    encrypted = encrypt_profile(message)

    print(f'\n\n{size}b DES Decrypt Memory Data:')
    decrypt_profile(encrypted)

    print(f'\n\n{size}b DES Encryption cProfile Data:')
    cProfile.runctx('des_encrypt(message)',{'des_encrypt':des_encrypt,'message':message},{})

    print(f'\n\n{size}b DES Decryption cProfile Data:')
    cProfile.runctx('des_decrypt(message)',{'des_decrypt':des_decrypt,'message':encrypted},{})

if __name__ == "__main__":
    main()