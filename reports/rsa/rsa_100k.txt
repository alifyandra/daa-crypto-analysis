100kb RSA Encrypt Memory Data:
Filename: /Users/alifyandra/Documents/SEM4/DAA/paper/testpy/rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   101     17.1 MiB     17.1 MiB           1   @profile
   102                                         def encrypt_profile(message):
   103     19.2 MiB      2.0 MiB           1       return rsa_encrypt(message)




100kb RSA Decrypt Memory Data:
Filename: /Users/alifyandra/Documents/SEM4/DAA/paper/testpy/rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   105     19.2 MiB     19.2 MiB           1   @profile
   106                                         def decrypt_profile(message):
   107     18.6 MiB     -0.6 MiB           1       return rsa_decrypt(message)




100kb RSA Encryption cProfile Data:
         344353 function calls (344350 primitive calls) in 0.481 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1033(_handle_fromlist)
        1    0.000    0.000    0.481    0.481 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
      512    0.001    0.000    0.135    0.000 PKCS1_OAEP.py:71(<lambda>)
      256    0.003    0.000    0.478    0.002 PKCS1_OAEP.py:88(encrypt)
      256    0.000    0.000    0.009    0.000 RSA.py:107(n)
      256    0.001    0.000    0.062    0.000 RSA.py:147(_encrypt)
        1    0.000    0.000    0.000    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.000    0.000 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.000    0.000 RSA.py:618(_import_pkcs1_private)
        2    0.000    0.000    0.001    0.000 RSA.py:638(_import_pkcs1_public)
        1    0.000    0.000    0.001    0.001 RSA.py:647(_import_subjectPublicKeyInfo)
        1    0.000    0.000    0.001    0.001 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.001    0.001 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
     6912    0.003    0.000    0.025    0.000 SHA1.py:142(new)
     6912    0.008    0.000    0.062    0.000 SHA1.py:148(new)
    13824    0.025    0.000    0.046    0.000 SHA1.py:72(__init__)
     6656    0.011    0.000    0.018    0.000 SHA1.py:83(update)
     6912    0.012    0.000    0.024    0.000 SHA1.py:97(digest)
      776    0.000    0.000    0.001    0.000 _IntegerGMP.py:115(new_mpz)
      776    0.021    0.000    0.023    0.000 _IntegerGMP.py:150(__init__)
      512    0.016    0.000    0.018    0.000 _IntegerGMP.py:172(__int__)
      259    0.000    0.000    0.013    0.000 _IntegerGMP.py:257(_apply_and_return)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:267(__ne__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:275(__le__)
      256    0.000    0.000    0.013    0.000 _IntegerGMP.py:278(__gt__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:281(__ge__)
      257    0.000    0.000    0.000    0.000 _IntegerGMP.py:284(__nonzero__)
      512    0.000    0.000    0.000    0.000 _IntegerGMP.py:288(is_negative)
      256    0.025    0.000    0.026    0.000 _IntegerGMP.py:355(inplace_pow)
      256    0.000    0.000    0.027    0.000 _IntegerGMP.py:394(__pow__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
      774    0.001    0.000    0.001    0.000 _IntegerGMP.py:699(__del__)
        1    0.000    0.000    0.000    0.000 __init__.py:25(_expand_subject_public_key_info)
     7936    0.009    0.000    0.012    0.000 __init__.py:48(create_string_buffer)
      512    0.001    0.000    0.001    0.000 _raw_api.py:182(get_c_string)
     7424    0.002    0.000    0.002    0.000 _raw_api.py:185(get_raw_buffer)
     8192    0.003    0.000    0.006    0.000 _raw_api.py:219(c_uint8_ptr)
    13824    0.005    0.000    0.005    0.000 _raw_api.py:239(__init__)
    13824    0.002    0.000    0.002    0.000 _raw_api.py:242(get)
    13824    0.004    0.000    0.006    0.000 _raw_api.py:245(address_of)
    13824    0.005    0.000    0.010    0.000 _raw_api.py:248(VoidPointer)
    13824    0.003    0.000    0.003    0.000 _raw_api.py:258(__init__)
    13568    0.002    0.000    0.002    0.000 _raw_api.py:262(get)
    13824    0.010    0.000    0.010    0.000 _raw_api.py:269(__del__)
      260    0.000    0.000    0.000    0.000 abc.py:96(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       20    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/8    0.000    0.000    0.001    0.000 asn1.py:199(decode)
       20    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        2    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        2    0.000    0.000    0.000    0.000 asn1.py:308(decode)
        2    0.000    0.000    0.000    0.000 asn1.py:321(_decodeFromStream)
       24    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        5    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        7    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:426(__len__)
        2    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        2    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        2    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       16    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        5    0.000    0.000    0.001    0.000 asn1.py:480(decode)
        5    0.000    0.000    0.001    0.000 asn1.py:509(_decodeFromStream)
       10    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
       10    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       33    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
        1    0.000    0.000    0.000    0.000 asn1.py:595(__init__)
       79    0.000    0.000    0.000    0.000 asn1.py:61(read)
        1    0.000    0.000    0.000    0.000 asn1.py:631(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:666(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:682(_decodeFromStream)
        1    0.000    0.000    0.000    0.000 asn1.py:690(<listcomp>)
       49    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
        1    0.000    0.000    0.000    0.000 asn1.py:731(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:760(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:775(_decodeFromStream)
       20    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
      768    0.001    0.000    0.001    0.000 number.py:37(ceil_div)
     6912    0.029    0.000    0.037    0.000 number.py:377(long_to_bytes)
      266    0.017    0.000    0.022    0.000 number.py:419(bytes_to_long)
      256    0.216    0.001    0.216    0.001 number.py:50(size)
      512    0.013    0.000    0.134    0.000 pss.py:163(MGF1)
      578    0.000    0.000    0.000    0.000 py3compat.py:115(bord)
      520    0.001    0.000    0.001    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
     8202    0.002    0.000    0.003    0.000 py3compat.py:128(byte_string)
     1034    0.000    0.000    0.000    0.000 py3compat.py:137(is_native_int)
      257    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:250(compile)
        2    0.000    0.000    0.000    0.000 re.py:289(_compile)
        1    0.001    0.001    0.481    0.481 rsa.py:79(rsa_encrypt)
      512    0.002    0.000    0.004    0.000 strxor.py:47(strxor)
      260    0.000    0.000    0.000    0.000 {built-in method _abc._abc_instancecheck}
    14600    0.003    0.000    0.003    0.000 {built-in method _ctypes.byref}
    38912    0.006    0.000    0.006    0.000 {built-in method _struct.pack}
    32778    0.004    0.000    0.004    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
      769    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.481    0.481 {built-in method builtins.exec}
       23    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
    29248    0.004    0.000    0.004    0.000 {built-in method builtins.isinstance}
30348/30347    0.003    0.000    0.003    0.000 {built-in method builtins.len}
      256    0.000    0.000    0.027    0.000 {built-in method builtins.pow}
        4    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
      256    0.001    0.000    0.001    0.000 {built-in method posix.urandom}
     7936    0.001    0.000    0.001    0.000 {built-in method sys.audit}
      271    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
      520    0.000    0.000    0.000    0.000 {method 'encode' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        2    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}




100kb RSA Decryption cProfile Data:
         700052 function calls (700042 primitive calls) in 2.353 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1033(_handle_fromlist)
        1    0.000    0.000    2.353    2.353 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
      256    0.009    0.000    2.174    0.008 PKCS1_OAEP.py:142(decrypt)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
      512    0.001    0.000    0.140    0.000 PKCS1_OAEP.py:71(<lambda>)
        2    0.020    0.010    0.161    0.081 Primality.py:119(lucas_test)
        4    0.000    0.000    0.000    0.000 Primality.py:144(alternate)
        2    0.000    0.000    0.174    0.087 Primality.py:222(test_probable_prime)
       20    0.000    0.000    0.000    0.000 Primality.py:267(<lambda>)
        2    0.000    0.000    0.012    0.006 Primality.py:45(miller_rabin_test)
      256    0.000    0.000    0.009    0.000 RSA.py:107(n)
        1    0.000    0.000    0.000    0.000 RSA.py:133(u)
      256    0.006    0.000    1.723    0.007 RSA.py:152(_decrypt)
      258    0.000    0.000    0.000    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.174    0.174 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.177    0.177 RSA.py:618(_import_pkcs1_private)
        1    0.000    0.000    0.177    0.177 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.177    0.177 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
     6912    0.003    0.000    0.025    0.000 SHA1.py:142(new)
     6912    0.008    0.000    0.063    0.000 SHA1.py:148(new)
    13824    0.025    0.000    0.047    0.000 SHA1.py:72(__init__)
     6656    0.011    0.000    0.019    0.000 SHA1.py:83(update)
     6912    0.012    0.000    0.026    0.000 SHA1.py:97(digest)
      362    0.002    0.000    0.010    0.000 _IntegerBase.py:297(random)
      260    0.003    0.000    0.034    0.000 _IntegerBase.py:345(random_range)
    11369    0.005    0.000    0.007    0.000 _IntegerGMP.py:115(new_mpz)
    11369    0.066    0.000    0.092    0.000 _IntegerGMP.py:150(__init__)
      516    0.017    0.000    0.019    0.000 _IntegerGMP.py:172(__int__)
      362    0.002    0.000    0.004    0.000 _IntegerGMP.py:234(from_bytes)
     5659    0.007    0.000    0.062    0.000 _IntegerGMP.py:257(_apply_and_return)
      308    0.000    0.000    0.001    0.000 _IntegerGMP.py:262(__eq__)
      261    0.001    0.000    0.014    0.000 _IntegerGMP.py:267(__ne__)
     4358    0.004    0.000    0.041    0.000 _IntegerGMP.py:272(__lt__)
      371    0.000    0.000    0.001    0.000 _IntegerGMP.py:275(__le__)
      256    0.000    0.000    0.014    0.000 _IntegerGMP.py:278(__gt__)
      369    0.000    0.000    0.004    0.000 _IntegerGMP.py:281(__ge__)
     1031    0.001    0.000    0.001    0.000 _IntegerGMP.py:284(__nonzero__)
     2058    0.001    0.000    0.001    0.000 _IntegerGMP.py:288(is_negative)
      518    0.001    0.000    0.005    0.000 _IntegerGMP.py:292(__add__)
      788    0.002    0.000    0.013    0.000 _IntegerGMP.py:304(__sub__)
     1028    0.004    0.000    0.010    0.000 _IntegerGMP.py:316(__mul__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:328(__floordiv__)
      772    0.005    0.000    0.009    0.000 _IntegerGMP.py:340(__mod__)
     1030    1.604    0.002    1.606    0.002 _IntegerGMP.py:355(inplace_pow)
     1030    0.001    0.000    1.612    0.002 _IntegerGMP.py:394(__pow__)
     6140    0.005    0.000    0.007    0.000 _IntegerGMP.py:421(__iadd__)
    12282    0.018    0.000    0.022    0.000 _IntegerGMP.py:457(__imul__)
    12294    0.027    0.000    0.028    0.000 _IntegerGMP.py:476(__imod__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
     8204    0.008    0.000    0.008    0.000 _IntegerGMP.py:523(__irshift__)
     4094    0.006    0.000    0.044    0.000 _IntegerGMP.py:553(get_bit)
     8200    0.005    0.000    0.005    0.000 _IntegerGMP.py:567(is_odd)
       12    0.000    0.000    0.000    0.000 _IntegerGMP.py:570(is_even)
      264    0.000    0.000    0.003    0.000 _IntegerGMP.py:573(size_in_bits)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:584(is_perfect_square)
     6147    0.010    0.000    0.013    0.000 _IntegerGMP.py:601(multiply_accumulate)
    16376    0.011    0.000    0.013    0.000 _IntegerGMP.py:623(set)
      257    0.015    0.000    0.015    0.000 _IntegerGMP.py:632(inplace_inverse)
      257    0.000    0.000    0.016    0.000 _IntegerGMP.py:656(inverse)
        3    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:686(jacobi_symbol)
    11361    0.010    0.000    0.010    0.000 _IntegerGMP.py:699(__del__)
      724    0.000    0.000    0.003    0.000 __init__.py:29(read)
      258    0.000    0.000    0.000    0.000 __init__.py:46(new)
     8196    0.011    0.000    0.014    0.000 __init__.py:48(create_string_buffer)
      516    0.001    0.000    0.001    0.000 _raw_api.py:182(get_c_string)
     7680    0.002    0.000    0.002    0.000 _raw_api.py:185(get_raw_buffer)
     8960    0.003    0.000    0.009    0.000 _raw_api.py:219(c_uint8_ptr)
    13824    0.005    0.000    0.005    0.000 _raw_api.py:239(__init__)
    13824    0.002    0.000    0.002    0.000 _raw_api.py:242(get)
    13824    0.004    0.000    0.006    0.000 _raw_api.py:245(address_of)
    13824    0.005    0.000    0.010    0.000 _raw_api.py:248(VoidPointer)
    13824    0.003    0.000    0.003    0.000 _raw_api.py:258(__init__)
    13568    0.002    0.000    0.002    0.000 _raw_api.py:262(get)
    13824    0.011    0.000    0.011    0.000 _raw_api.py:269(__del__)
     6620    0.002    0.000    0.005    0.000 abc.py:96(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       19    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/1    0.000    0.000    0.003    0.003 asn1.py:199(decode)
       19    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        9    0.000    0.000    0.002    0.000 asn1.py:308(decode)
        9    0.002    0.000    0.002    0.000 asn1.py:321(_decodeFromStream)
       29    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        1    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        4    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        1    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        1    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       11    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        1    0.000    0.000    0.003    0.003 asn1.py:480(decode)
        1    0.000    0.000    0.002    0.002 asn1.py:509(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
        9    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       20    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
       72    0.000    0.000    0.000    0.000 asn1.py:61(read)
       38    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
       19    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
      768    0.001    0.000    0.001    0.000 number.py:37(ceil_div)
     6912    0.032    0.000    0.050    0.000 number.py:377(long_to_bytes)
      271    0.018    0.000    0.023    0.000 number.py:419(bytes_to_long)
      256    0.220    0.001    0.220    0.001 number.py:50(size)
      512    0.014    0.000    0.139    0.000 pss.py:163(MGF1)
      362    0.001    0.000    0.001    0.000 py3compat.py:108(bchr)
    20913    0.002    0.000    0.002    0.000 py3compat.py:115(bord)
     9816    0.008    0.000    0.013    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
     8970    0.004    0.000    0.005    0.000 py3compat.py:128(byte_string)
    37521    0.009    0.000    0.013    0.000 py3compat.py:137(is_native_int)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:250(compile)
        2    0.000    0.000    0.000    0.000 re.py:289(_compile)
        1    0.002    0.002    2.353    2.353 rsa.py:90(rsa_decrypt)
      768    0.003    0.000    0.007    0.000 strxor.py:47(strxor)
     6620    0.003    0.000    0.003    0.000 {built-in method _abc._abc_instancecheck}
    25193    0.005    0.000    0.005    0.000 {built-in method _ctypes.byref}
    38912    0.006    0.000    0.006    0.000 {built-in method _struct.pack}
    32783    0.004    0.000    0.004    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
      768    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    2.353    2.353 {built-in method builtins.exec}
  282/281    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
   151477    0.019    0.000    0.024    0.000 {built-in method builtins.isinstance}
    31963    0.003    0.000    0.003    0.000 {built-in method builtins.len}
     1030    0.001    0.000    1.613    0.002 {built-in method builtins.pow}
       12    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
      724    0.003    0.000    0.003    0.000 {built-in method posix.urandom}
     8196    0.001    0.000    0.001    0.000 {built-in method sys.audit}
      265    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
     9816    0.002    0.000    0.002    0.000 {method 'encode' of 'str' objects}
      256    0.000    0.000    0.000    0.000 {method 'find' of 'bytes' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
     2126    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}


