100kb Blowfish Encrypt Memory Data:
Filename: /Users/alifyandra/Documents/SEM4/DAA/paper/testpy/blowfish.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    19     16.7 MiB     16.7 MiB           1   @profile
    20                                         def encrypt_profile(message):
    21     16.8 MiB      0.1 MiB           1       return blowfish_encrypt(message)




100kb Blowfish Decrypt Memory Data:
Filename: /Users/alifyandra/Documents/SEM4/DAA/paper/testpy/blowfish.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    23     16.8 MiB     16.8 MiB           1   @profile
    24                                         def decrypt_profile(message):
    25     16.9 MiB      0.1 MiB           1       return blowfish_decrypt(message)




100kb Blowfish Encryption cProfile Data:
         61 function calls in 0.008 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.008    0.008 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 Blowfish.py:60(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 Blowfish.py:86(new)
        1    0.000    0.000    0.000    0.000 __init__.py:48(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.008    0.008    0.008    0.008 _mode_cfb.py:124(encrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.000    0.000    0.000    0.000 _raw_api.py:185(get_raw_buffer)
        4    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
        1    0.000    0.000    0.008    0.008 blowfish.py:9(blowfish_encrypt)
        4    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.008    0.008 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        8    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {built-in method sys.audit}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        5    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}




100kb Blowfish Decryption cProfile Data:
         61 function calls in 0.008 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.008    0.008 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 Blowfish.py:60(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 Blowfish.py:86(new)
        1    0.000    0.000    0.000    0.000 __init__.py:48(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.007    0.007    0.008    0.008 _mode_cfb.py:185(decrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.000    0.000    0.000    0.000 _raw_api.py:185(get_raw_buffer)
        4    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
        1    0.000    0.000    0.008    0.008 blowfish.py:14(blowfish_decrypt)
        4    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.008    0.008 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        8    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {built-in method sys.audit}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        5    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}


