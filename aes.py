from memory_profiler import profile
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
import cProfile, os, sys
key = get_random_bytes(16)
IV = get_random_bytes(16)

def aes_encrypt(message):
    mode = AES.MODE_CFB
    encryptor = AES.new(key, mode,IV=IV)
    ciphertext = encryptor.encrypt(message)
    return ciphertext

def aes_decrypt(ciphertext):
    mode = AES.MODE_CFB
    decryptor = AES.new(key, mode,IV=IV)
    plaintext = decryptor.decrypt(ciphertext)
    return plaintext

@profile
def encrypt_profile(message):
    return aes_encrypt(message)

@profile
def decrypt_profile(message):
    return aes_decrypt(message)

def main():
    size = sys.argv[1]
    message = open(os.path.join('textfiles',size+'.txt'),'r').read().encode('utf-8')

    print(f'{size}b AES Encrypt Memory Data:')
    encrypted = encrypt_profile(message)

    print(f'\n\n{size}b AES Decrypt Memory Data:')
    decrypt_profile(encrypted)

    print(f'\n\n{size}b AES Encryption cProfile Data:')
    cProfile.runctx('aes_encrypt(message)',{'aes_encrypt':aes_encrypt,'message':message},{})

    print(f'\n\n{size}b AES Decryption cProfile Data:')
    cProfile.runctx('aes_decrypt(message)',{'aes_decrypt':aes_decrypt,'message':encrypted},{})

if __name__ == "__main__":
    main()