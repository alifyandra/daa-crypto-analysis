from memory_profiler import profile
from Crypto.Cipher import DES3
from Crypto.Random import get_random_bytes
import cProfile, os, sys

def des3_encrypt(message):
    while True:
        try:
            key = DES3.adjust_key_parity(get_random_bytes(16))
            break
        except ValueError:
            pass
    mode = DES3.MODE_CFB
    encryptor = DES3.new(key, mode)
    ciphertext = encryptor.encrypt(message)
    return ciphertext

def des3_decrypt(ciphertext):
    while True:
        try:
            key = DES3.adjust_key_parity(get_random_bytes(16))
            break
        except ValueError:
            pass
    mode = DES3.MODE_CFB
    decryptor = DES3.new(key, mode)
    plaintext = decryptor.decrypt(ciphertext)
    return plaintext

@profile
def encrypt_profile(message):
    return des3_encrypt(message)

@profile
def decrypt_profile(message):
    return des3_decrypt(message)

def main():
    size = sys.argv[1]
    message = open(os.path.join('textfiles',size+'.txt'),'r').read().encode('utf-8')

    print(f'{size}b 3DES Encrypt Memory Data:')
    encrypted = encrypt_profile(message)

    print(f'\n\n{size}b 3DES Decrypt Memory Data:')
    decrypt_profile(encrypted)

    print(f'\n\n{size}b 3DES Encryption cProfile Data:')
    cProfile.runctx('des3_encrypt(message)',{'des3_encrypt':des3_encrypt,'message':message},{})

    print(f'\n\n{size}b 3DES Decryption cProfile Data:')
    cProfile.runctx('des3_decrypt(message)',{'des3_decrypt':des3_decrypt,'message':encrypted},{})


if __name__ == "__main__":
    main()