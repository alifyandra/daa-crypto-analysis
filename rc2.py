from memory_profiler import profile
from Crypto.Cipher import ARC2
from Crypto.Random import get_random_bytes
import cProfile, os, sys

key = get_random_bytes(16)
iv = get_random_bytes(8)

def rc2_encrypt(message):
    cipher = ARC2.new(key, ARC2.MODE_CFB, iv)
    encryption = cipher.encrypt(message)
    return encryption

def rc2_decrypt(ciphertext):
    cipher = ARC2.new(key, ARC2.MODE_CFB, iv)
    decryption = cipher.decrypt(ciphertext)
    return decryption

@profile
def encrypt_profile(message):
    return rc2_encrypt(message)

@profile
def decrypt_profile(message):
    return rc2_decrypt(message)

def main():
    size = sys.argv[1]
    message = open(os.path.join('textfiles',size+'.txt'),'r').read().encode('utf-8')

    print(f'{size}b RC2 Encrypt Memory Data:')
    encrypted = encrypt_profile(message)

    print(f'\n\n{size}b RC2 Decrypt Memory Data:')
    decrypt_profile(encrypted)

    print(f'\n\n{size}b RC2 Encryption cProfile Data:')
    cProfile.runctx('rc2_encrypt(message)',{'rc2_encrypt':rc2_encrypt,'message':message},{})

    print(f'\n\n{size}b RC2 Decryption cProfile Data:')
    cProfile.runctx('rc2_decrypt(message)',{'rc2_decrypt':rc2_decrypt,'message':encrypted},{})

if __name__ == "__main__":
    main()