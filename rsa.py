from memory_profiler import profile
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto import Random
import cProfile, os, sys

# random_generator = Random.new().read
# key = RSA.generate(1024, random_generator)

# publickey = PKCS1_OAEP.new(key.publickey)
# privatekey = PKCS1_OAEP.new(key)

pub_key_str = """-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtdBa7DLPPlNcyfN/me04
LIppttUUMYFSuXqfenABryssNPGgz7qAQI5RgdUSfQ4ZutLi8k5CWAB9JKcCzfJd
K8H4BS9pRyWn7gnZpXuw7xcPHHqgSxM77BbwUD4nG9NEEvIluSQF1qYBZGlUk6+1
sdXsVzjBkA3/9+3hWE2WbAW0Pi8MA4WGVWjLFCZA5ORbrhPGCdmN48GBUByHnAmY
hOeQRkTtKPj1HqEdhEFpwsGTF6SYS1Izb2Kz7Lr9o+pFYpSGNQo76bbNT+PRRntO
s1ALM4OcZfhkifXR/VpFFWkAluzM/NjbQAfrYDlKMDW528IfizusCr2iUei1jkaW
7MkCK1owA0LNwZad35Ram6KT8Hpig4ukj84BVYvKxjq4XqKuHD1u1A1ouJWmH+Lj
Ezq76ztw9Z6JIJ2tJoAJ4LDN0g/Bf4OL44HTFIHIitv6GpgIHwHyl1Bx5MRGvQWd
OgQ8vjnfCydg9lOTknnjcnID70VXhuQ1lQEYxN5qPzSqWo99p0kl9kZ7U45uJgY0
MTpOqm8xM4tu+CxuBdce67EX7CngK6mVKA/v37700sOztalIWD+gsngf5sDxihsR
YTmybZsO+nwUmdGPk0GS2WyPohVnifmARkmu4yhDwG+5WsxXeWhFNbc55pbc3fV8
Hbop8S+6HG8+EO7jUitwzsMCAwEAAQ==
-----END PUBLIC KEY-----"""
priv_key_str = """-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAtdBa7DLPPlNcyfN/me04LIppttUUMYFSuXqfenABryssNPGg
z7qAQI5RgdUSfQ4ZutLi8k5CWAB9JKcCzfJdK8H4BS9pRyWn7gnZpXuw7xcPHHqg
SxM77BbwUD4nG9NEEvIluSQF1qYBZGlUk6+1sdXsVzjBkA3/9+3hWE2WbAW0Pi8M
A4WGVWjLFCZA5ORbrhPGCdmN48GBUByHnAmYhOeQRkTtKPj1HqEdhEFpwsGTF6SY
S1Izb2Kz7Lr9o+pFYpSGNQo76bbNT+PRRntOs1ALM4OcZfhkifXR/VpFFWkAluzM
/NjbQAfrYDlKMDW528IfizusCr2iUei1jkaW7MkCK1owA0LNwZad35Ram6KT8Hpi
g4ukj84BVYvKxjq4XqKuHD1u1A1ouJWmH+LjEzq76ztw9Z6JIJ2tJoAJ4LDN0g/B
f4OL44HTFIHIitv6GpgIHwHyl1Bx5MRGvQWdOgQ8vjnfCydg9lOTknnjcnID70VX
huQ1lQEYxN5qPzSqWo99p0kl9kZ7U45uJgY0MTpOqm8xM4tu+CxuBdce67EX7Cng
K6mVKA/v37700sOztalIWD+gsngf5sDxihsRYTmybZsO+nwUmdGPk0GS2WyPohVn
ifmARkmu4yhDwG+5WsxXeWhFNbc55pbc3fV8Hbop8S+6HG8+EO7jUitwzsMCAwEA
AQKCAgEAqVJTeumOzDKClalpyoZ77C8zaIxkc/5GdmpWn4Y0a1g/oUbuRscfrf9c
tRckUsaQ+8ZuiM6NRrud0Eahe/8c+iV4wAt5dkMBc9REl7LsAp5s+k7RfVl6UtLC
F1jqvyUvnp1YuGVt8otDfKoDlE7tXaMmqcw3p9sB6XnmhYAseImPZOIXxCfUBTLE
/gzqHsD1CNdtdbrgQ53wuhT3zj+5hPzLBkYmwCOq1E2j2Ym53yC7xicJlgwFztQT
UIKMuNzypW5A1qCjT3NpHd/DBGrNznzbEayeOPDtYPZIDDTHZbsWS3hZJ5kmBynY
8RIpn2BJqPnOurJl9NLKmpGok8n1vxWuRKI48492DFSHGD4nNQiLpoELaEJu6NO9
Dl508NcE2ogOPoSEm/yA3rpz0a5ee27H3wUfa9j3r9MWOtkWsDluJoozWKYzx3+k
g38haTYAFLlQR5zooqDtqlDtSBwXroGMkl20WEpbtglA33elI+DqEsOLk5Ye+FjI
jBsKlP35qasGTeJu5foKYBbjiA0mq7v32zXDKZ6CwBG0qD1b70hQaxVFzxCknuxh
CmTlgF1ywczNX98Xf4dodCnmykaPR5iwCtMPSOBteZ7t59yWjZq/hM7SrKpJTrnz
xS3tQCr6SjwD8CrDTXAjpb4AgWx7YV2nrSWBrxuFzaFf3lqhKSECggEBAPbYAs1S
dPR3DOrBh5JW5dTCwZ3n3Gjp313uYeU/JkzSjXYBEAjqgsO8Crx2IFmP7SIXeD5v
xmczlDFDyW8EcDdRzqJLa+Wp4LB/jE3fM3fI2CMRQ3ayFZNQnfgrNbIXEkNk1QC2
W0wY4Gttw/nMuycQ1Xs9MnlNe1RCzLvZPDeMoihbzEWv0wa1z7FQPasdD08O4MVN
//PUh4awUwE722WEAoodZ4ewvJLfCUf7GV8KAP4fmlS6HXbWV5M/Xz9qqvy3DimH
am0A77qzKK2xjZq84dHgw6tErcTjZy7fqXyTfob8hlRCD5xqqWkfOU1IF+EBiidl
B7B7Ug4e//Nt4IkCggEBALyO1Kbyp5AXa/jL6C4s3m6LA9c6Zdi3+W8ije4wOUJk
At2iPZQG0chB5tYMPH8undcyNjdFaiMgqZRgHGrIcpAXMF1fP0phF/wmoW3mM9nD
ksx0dW5PSAahkbK3Z4Ki4DvgfjtWR6ggewVezid76WPoCYznxjiL6gcpjRzkjAAI
WwqYi1vtFrQ27BsLVt/9lcG5+2Ksd6j3ay82jCExM8uiMVuSuwujYAzqn6lEPqUR
MVX90E/rvzaoIK4fvaFvXtpBjAIknhPmHPrrhWttfQoZSSAE3CVavTBrJry4u15h
LB0p+aKfoc7NgMS4hSLk2a1UncXOUkFCzb8QGz6/6esCggEAPrc1Rwt7MZ2Jx59y
HYk8jakJ3l/JjVJWcRwe/UrlJUGdtWyhsVI79RUyuR1eLMM3oaDK6Tom85OUREwP
wsgi5rs/P9bfzBM0RkOx1xstjXKXD5nURTUL7EjgSexYkS3xcvQMdkjeu1/arRDP
aQlOR6esRcqPp2g7PHZYZCXWedqeXJiuNspsWFkbMtbLRE121URXcWnW7RGDeYIV
JMwbexhmOMctxbjuwHYwenxevtng1LAc2wgKcmYVDBFe1tL4LIsp6v0/2gAR6KKa
ZD8nVjhFO7jangouCEcqsSscec1TqWJBkujR5JDpCA9p1KvLfVyRyF6P7JP4jiPs
LeIt2QKCAQBG6ZRyANohp51UoSL9kqRIgIF+RRkeavok4KLuDDbMkB4/yg9Mh1AH
9zjgERQDZL25u8mF+dYTxengwlCNnDrxk7b9tp9v0poZm1Gkqimf4RxpRfcP+EMH
M2Y/Bxinmaeo/SGcITOruCNr/p6bXM+GYjKNSnFSvNPTw72tOwqnzHyovHvUo5Kh
TfJ3rUkLziuvAsnW7zPFWN93jxUeOonXMHVNkh5EHzo1QVuwZdkFbTTXuS/JjsiH
vq0jrw47E05h2LrV532H0Eff2gmAQFoBHUdSAmHkrn5uIi4cVYEp2nOjuDCJB23r
/39I/HgdimGO/T6ksJZgssX78Au/Cp/nAoIBAQDy3gzTjZHKcAXF99G5xdpThvaB
8BaqI2ScThKA1l7OOi5OlFz8ocgKMFM5R4h8eLZrxofb7HcLjGrxYCCVFxx2l6Cr
KI1P14DZQZ162acfTa2wXpm1/Gfct3WLfKugcCVlJ6R93P3XzcVMIIPoFHpN68cl
WLFxEIoCks7URzzQPiLg+hJYvZr7FdZlkBPgIqcf1dA53TxMsdOnO/JweDqzk7rI
eCcB/aQWq+r4g6b1H2NWXXY4tDpSBHX24mtLjj5XeKk+ZOEdkgiXLL6HUx7NQ98r
b+sqGE9TY4Sp9XPX/Mv1L74+iIzOz56Q2Qh5ZZNCdvOmXcERzSdZNcyfylNJ
-----END RSA PRIVATE KEY-----"""

def rsa_encrypt(message):
    publickey = PKCS1_OAEP.new(RSA.importKey(pub_key_str))
    length = 400
    res = []
    for i in range(0, len(message), length):
        res.append(publickey.encrypt(message[i:i+length]))
    encryption = res[0]
    for i in range(1, len(res)):
        encryption += res[i]
    return encryption

def rsa_decrypt(ciphertext):
    privatekey = PKCS1_OAEP.new(RSA.importKey(priv_key_str))
    length = 512
    res = []
    for i in range(0, len(ciphertext), length):
        res.append(privatekey.decrypt(ciphertext[i:i+length]))
    decryption = res[0]
    for i in range(1, len(res)):
        decryption += res[i]
    return decryption

@profile
def encrypt_profile(message):
    return rsa_encrypt(message)

@profile
def decrypt_profile(message):
    return rsa_decrypt(message)

def main():
    size = sys.argv[1]
    message = open(os.path.join('textfiles',size+'.txt'),'r').read().encode('utf-8')

    print(f'{size}b RSA Encrypt Memory Data:')
    encrypted = encrypt_profile(message)

    print(f'\n\n{size}b RSA Decrypt Memory Data:')
    decrypt_profile(encrypted)

    print(f'\n\n{size}b RSA Encryption cProfile Data:')
    cProfile.runctx('rsa_encrypt(message)',{'rsa_encrypt':rsa_encrypt,'message':message},{})

    print(f'\n\n{size}b RSA Decryption cProfile Data:')
    cProfile.runctx('rsa_decrypt(message)',{'rsa_decrypt':rsa_decrypt,'message':encrypted},{})

if __name__ == "__main__":
    main()