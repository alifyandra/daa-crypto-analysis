1mb AES Encrypt Memory Data:
Filename: aes.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    20     20.8 MiB     20.8 MiB           1   @profile
    21                                         def encrypt_profile(message):
    22     22.7 MiB      1.9 MiB           1       return aes_encrypt(message)




1mb AES Decrypt Memory Data:
Filename: aes.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    24     22.7 MiB     22.7 MiB           1   @profile
    25                                         def decrypt_profile(message):
    26     23.7 MiB      1.0 MiB           1       return aes_decrypt(message)




1mb AES Encryption cProfile Data:
         61 function calls in 0.034 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.034    0.034 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 AES.py:130(new)
        1    0.000    0.000    0.000    0.000 AES.py:81(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 __init__.py:47(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.033    0.033    0.034    0.034 _mode_cfb.py:124(encrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.000    0.000    0.000    0.000 _raw_api.py:185(get_raw_buffer)
        4    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
        1    0.000    0.000    0.034    0.034 aes.py:8(aes_encrypt)
        4    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.034    0.034 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        7    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        6    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'update' of 'dict' objects}




1mb AES Decryption cProfile Data:
         61 function calls in 0.032 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.032    0.032 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 AES.py:130(new)
        1    0.000    0.000    0.000    0.000 AES.py:81(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 __init__.py:47(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.031    0.031    0.032    0.032 _mode_cfb.py:185(decrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.001    0.001    0.001    0.001 _raw_api.py:185(get_raw_buffer)
        4    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
        1    0.000    0.000    0.032    0.032 aes.py:14(aes_decrypt)
        4    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.032    0.032 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        7    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        6    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'update' of 'dict' objects}


