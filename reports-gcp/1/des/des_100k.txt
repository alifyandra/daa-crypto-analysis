100kb DES Encrypt Memory Data:
Filename: des.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    20     20.1 MiB     20.1 MiB           1   @profile
    21                                         def encrypt_profile(message):
    22     20.2 MiB      0.1 MiB           1       return des_encrypt(message)




100kb DES Decrypt Memory Data:
Filename: des.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    24     20.2 MiB     20.2 MiB           1   @profile
    25                                         def decrypt_profile(message):
    26     20.2 MiB      0.0 MiB           1       return des_decrypt(message)




100kb DES Encryption cProfile Data:
         59 function calls in 0.011 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.011    0.011 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 DES.py:60(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 DES.py:85(new)
        1    0.000    0.000    0.000    0.000 __init__.py:47(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.011    0.011    0.011    0.011 _mode_cfb.py:124(encrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.000    0.000    0.000    0.000 _raw_api.py:185(get_raw_buffer)
        4    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
        1    0.000    0.000    0.011    0.011 des.py:8(des_encrypt)
        4    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.011    0.011 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        7    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        5    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}




100kb DES Decryption cProfile Data:
         59 function calls in 0.011 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.011    0.011 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 DES.py:60(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 DES.py:85(new)
        1    0.000    0.000    0.000    0.000 __init__.py:47(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.010    0.010    0.010    0.010 _mode_cfb.py:185(decrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.000    0.000    0.000    0.000 _raw_api.py:185(get_raw_buffer)
        4    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
        1    0.000    0.000    0.011    0.011 des.py:14(des_decrypt)
        4    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.011    0.011 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        7    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        5    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}


