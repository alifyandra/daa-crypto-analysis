100kb RSA Encrypt Memory Data:
Filename: rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   101     20.5 MiB     20.5 MiB           1   @profile
   102                                         def encrypt_profile(message):
   103     21.4 MiB      0.9 MiB           1       return rsa_encrypt(message)




100kb RSA Decrypt Memory Data:
Filename: rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   105     21.4 MiB     21.4 MiB           1   @profile
   106                                         def decrypt_profile(message):
   107     21.7 MiB      0.3 MiB           1       return rsa_decrypt(message)




100kb RSA Encryption cProfile Data:
         336418 function calls (336415 primitive calls) in 0.378 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1009(_handle_fromlist)
        1    0.000    0.000    0.378    0.378 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
      512    0.000    0.000    0.107    0.000 PKCS1_OAEP.py:71(<lambda>)
      256    0.002    0.000    0.376    0.001 PKCS1_OAEP.py:88(encrypt)
      256    0.000    0.000    0.006    0.000 RSA.py:107(n)
      256    0.001    0.000    0.054    0.000 RSA.py:147(_encrypt)
        1    0.000    0.000    0.000    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.000    0.000 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.000    0.000 RSA.py:618(_import_pkcs1_private)
        2    0.000    0.000    0.001    0.000 RSA.py:638(_import_pkcs1_public)
        1    0.000    0.000    0.001    0.001 RSA.py:647(_import_subjectPublicKeyInfo)
        1    0.000    0.000    0.001    0.001 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.001    0.001 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
     6912    0.003    0.000    0.020    0.000 SHA1.py:142(new)
     6912    0.007    0.000    0.050    0.000 SHA1.py:148(new)
    13824    0.020    0.000    0.037    0.000 SHA1.py:72(__init__)
     6656    0.009    0.000    0.015    0.000 SHA1.py:83(update)
     6912    0.010    0.000    0.017    0.000 SHA1.py:97(digest)
      776    0.000    0.000    0.000    0.000 _IntegerGMP.py:115(new_mpz)
      776    0.018    0.000    0.020    0.000 _IntegerGMP.py:150(__init__)
      512    0.011    0.000    0.012    0.000 _IntegerGMP.py:172(__int__)
      259    0.000    0.000    0.011    0.000 _IntegerGMP.py:257(_apply_and_return)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:267(__ne__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:275(__le__)
      256    0.000    0.000    0.011    0.000 _IntegerGMP.py:278(__gt__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:281(__ge__)
      257    0.000    0.000    0.000    0.000 _IntegerGMP.py:284(__nonzero__)
      512    0.000    0.000    0.000    0.000 _IntegerGMP.py:288(is_negative)
      256    0.024    0.000    0.025    0.000 _IntegerGMP.py:355(inplace_pow)
      256    0.000    0.000    0.026    0.000 _IntegerGMP.py:394(__pow__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
      774    0.001    0.000    0.001    0.000 _IntegerGMP.py:699(__del__)
        1    0.000    0.000    0.000    0.000 __init__.py:25(_expand_subject_public_key_info)
     7936    0.005    0.000    0.007    0.000 __init__.py:47(create_string_buffer)
      512    0.000    0.000    0.000    0.000 _raw_api.py:182(get_c_string)
     7424    0.001    0.000    0.001    0.000 _raw_api.py:185(get_raw_buffer)
     8192    0.002    0.000    0.005    0.000 _raw_api.py:219(c_uint8_ptr)
    13824    0.003    0.000    0.003    0.000 _raw_api.py:239(__init__)
    13824    0.002    0.000    0.002    0.000 _raw_api.py:242(get)
    13824    0.003    0.000    0.006    0.000 _raw_api.py:245(address_of)
    13824    0.004    0.000    0.007    0.000 _raw_api.py:248(VoidPointer)
    13824    0.002    0.000    0.002    0.000 _raw_api.py:258(__init__)
    13568    0.001    0.000    0.001    0.000 _raw_api.py:262(get)
    13824    0.007    0.000    0.007    0.000 _raw_api.py:269(__del__)
      260    0.000    0.000    0.000    0.000 abc.py:137(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       20    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/8    0.000    0.000    0.001    0.000 asn1.py:199(decode)
       20    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        2    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        2    0.000    0.000    0.000    0.000 asn1.py:308(decode)
        2    0.000    0.000    0.000    0.000 asn1.py:321(_decodeFromStream)
       24    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        5    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        7    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:426(__len__)
        2    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        2    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        2    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       16    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        5    0.000    0.000    0.001    0.000 asn1.py:480(decode)
        5    0.000    0.000    0.000    0.000 asn1.py:509(_decodeFromStream)
       10    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
       10    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       33    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
        1    0.000    0.000    0.000    0.000 asn1.py:595(__init__)
       79    0.000    0.000    0.000    0.000 asn1.py:61(read)
        1    0.000    0.000    0.000    0.000 asn1.py:631(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:666(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:682(_decodeFromStream)
        1    0.000    0.000    0.000    0.000 asn1.py:690(<listcomp>)
       49    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
        1    0.000    0.000    0.000    0.000 asn1.py:731(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:760(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:775(_decodeFromStream)
       20    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
      768    0.001    0.000    0.001    0.000 number.py:37(ceil_div)
     6912    0.022    0.000    0.028    0.000 number.py:377(long_to_bytes)
      266    0.013    0.000    0.017    0.000 number.py:419(bytes_to_long)
      256    0.167    0.001    0.167    0.001 number.py:50(size)
      512    0.011    0.000    0.106    0.000 pss.py:163(MGF1)
      578    0.000    0.000    0.000    0.000 py3compat.py:115(bord)
      520    0.001    0.000    0.001    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
     8202    0.002    0.000    0.003    0.000 py3compat.py:128(byte_string)
     1034    0.000    0.000    0.000    0.000 py3compat.py:137(is_native_int)
      257    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:232(compile)
        2    0.000    0.000    0.000    0.000 re.py:271(_compile)
        1    0.001    0.001    0.378    0.378 rsa.py:79(rsa_encrypt)
      512    0.002    0.000    0.003    0.000 strxor.py:47(strxor)
      260    0.000    0.000    0.000    0.000 {built-in method _abc._abc_instancecheck}
    14600    0.002    0.000    0.002    0.000 {built-in method _ctypes.byref}
    38912    0.004    0.000    0.004    0.000 {built-in method _struct.pack}
    32778    0.004    0.000    0.004    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
      769    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.378    0.378 {built-in method builtins.exec}
       24    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
    29248    0.003    0.000    0.003    0.000 {built-in method builtins.isinstance}
30348/30347    0.002    0.000    0.002    0.000 {built-in method builtins.len}
      256    0.000    0.000    0.026    0.000 {built-in method builtins.pow}
        4    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
      256    0.000    0.000    0.000    0.000 {built-in method posix.urandom}
      271    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
      520    0.000    0.000    0.000    0.000 {method 'encode' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        2    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}




100kb RSA Decryption cProfile Data:
         692245 function calls (692235 primitive calls) in 3.340 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1009(_handle_fromlist)
        1    0.000    0.000    3.340    3.340 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
      256    0.009    0.000    3.195    0.012 PKCS1_OAEP.py:142(decrypt)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
      512    0.001    0.000    0.156    0.000 PKCS1_OAEP.py:71(<lambda>)
        2    0.018    0.009    0.129    0.064 Primality.py:119(lucas_test)
        4    0.000    0.000    0.000    0.000 Primality.py:144(alternate)
        2    0.000    0.000    0.141    0.071 Primality.py:222(test_probable_prime)
       20    0.000    0.000    0.000    0.000 Primality.py:267(<lambda>)
        2    0.000    0.000    0.012    0.006 Primality.py:45(miller_rabin_test)
      256    0.000    0.000    0.009    0.000 RSA.py:107(n)
        1    0.000    0.000    0.000    0.000 RSA.py:133(u)
      256    0.006    0.000    2.659    0.010 RSA.py:152(_decrypt)
      258    0.000    0.000    0.000    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.142    0.142 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.143    0.143 RSA.py:618(_import_pkcs1_private)
        1    0.000    0.000    0.143    0.143 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.144    0.144 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
     6912    0.004    0.000    0.029    0.000 SHA1.py:142(new)
     6912    0.010    0.000    0.074    0.000 SHA1.py:148(new)
    13824    0.029    0.000    0.053    0.000 SHA1.py:72(__init__)
     6656    0.013    0.000    0.021    0.000 SHA1.py:83(update)
     6912    0.014    0.000    0.026    0.000 SHA1.py:97(digest)
      371    0.002    0.000    0.010    0.000 _IntegerBase.py:297(random)
      260    0.004    0.000    0.037    0.000 _IntegerBase.py:345(random_range)
    11387    0.004    0.000    0.007    0.000 _IntegerGMP.py:115(new_mpz)
    11387    0.064    0.000    0.092    0.000 _IntegerGMP.py:150(__init__)
      516    0.017    0.000    0.020    0.000 _IntegerGMP.py:172(__int__)
      371    0.002    0.000    0.005    0.000 _IntegerGMP.py:234(from_bytes)
     5675    0.006    0.000    0.056    0.000 _IntegerGMP.py:257(_apply_and_return)
      306    0.000    0.000    0.001    0.000 _IntegerGMP.py:262(__eq__)
      261    0.001    0.000    0.015    0.000 _IntegerGMP.py:267(__ne__)
     4358    0.003    0.000    0.032    0.000 _IntegerGMP.py:272(__lt__)
      380    0.000    0.000    0.001    0.000 _IntegerGMP.py:275(__le__)
      256    0.000    0.000    0.015    0.000 _IntegerGMP.py:278(__gt__)
      378    0.000    0.000    0.004    0.000 _IntegerGMP.py:281(__ge__)
     1031    0.001    0.000    0.001    0.000 _IntegerGMP.py:284(__nonzero__)
     2058    0.001    0.000    0.001    0.000 _IntegerGMP.py:288(is_negative)
      518    0.001    0.000    0.006    0.000 _IntegerGMP.py:292(__add__)
      788    0.002    0.000    0.013    0.000 _IntegerGMP.py:304(__sub__)
     1028    0.004    0.000    0.011    0.000 _IntegerGMP.py:316(__mul__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:328(__floordiv__)
      772    0.006    0.000    0.010    0.000 _IntegerGMP.py:340(__mod__)
     1030    2.524    0.002    2.527    0.002 _IntegerGMP.py:355(inplace_pow)
     1030    0.001    0.000    2.533    0.002 _IntegerGMP.py:394(__pow__)
     6140    0.004    0.000    0.006    0.000 _IntegerGMP.py:421(__iadd__)
    12282    0.014    0.000    0.018    0.000 _IntegerGMP.py:457(__imul__)
    12294    0.021    0.000    0.022    0.000 _IntegerGMP.py:476(__imod__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
     8204    0.007    0.000    0.007    0.000 _IntegerGMP.py:523(__irshift__)
     4094    0.005    0.000    0.033    0.000 _IntegerGMP.py:553(get_bit)
     8200    0.004    0.000    0.004    0.000 _IntegerGMP.py:567(is_odd)
       12    0.000    0.000    0.000    0.000 _IntegerGMP.py:570(is_even)
      264    0.000    0.000    0.003    0.000 _IntegerGMP.py:573(size_in_bits)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:584(is_perfect_square)
     6147    0.007    0.000    0.010    0.000 _IntegerGMP.py:601(multiply_accumulate)
    16376    0.009    0.000    0.010    0.000 _IntegerGMP.py:623(set)
      257    0.021    0.000    0.021    0.000 _IntegerGMP.py:632(inplace_inverse)
      257    0.000    0.000    0.023    0.000 _IntegerGMP.py:656(inverse)
        3    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:686(jacobi_symbol)
    11379    0.010    0.000    0.010    0.000 _IntegerGMP.py:699(__del__)
      742    0.000    0.000    0.003    0.000 __init__.py:29(read)
      258    0.000    0.000    0.000    0.000 __init__.py:46(new)
     8196    0.009    0.000    0.012    0.000 __init__.py:47(create_string_buffer)
      516    0.001    0.000    0.001    0.000 _raw_api.py:182(get_c_string)
     7680    0.002    0.000    0.002    0.000 _raw_api.py:185(get_raw_buffer)
     8960    0.004    0.000    0.008    0.000 _raw_api.py:219(c_uint8_ptr)
    13824    0.005    0.000    0.005    0.000 _raw_api.py:239(__init__)
    13824    0.002    0.000    0.002    0.000 _raw_api.py:242(get)
    13824    0.005    0.000    0.008    0.000 _raw_api.py:245(address_of)
    13824    0.006    0.000    0.011    0.000 _raw_api.py:248(VoidPointer)
    13824    0.004    0.000    0.004    0.000 _raw_api.py:258(__init__)
    13568    0.002    0.000    0.002    0.000 _raw_api.py:262(get)
    13824    0.011    0.000    0.011    0.000 _raw_api.py:269(__del__)
     6629    0.002    0.000    0.004    0.000 abc.py:137(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       19    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/1    0.000    0.000    0.001    0.001 asn1.py:199(decode)
       19    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        9    0.000    0.000    0.001    0.000 asn1.py:308(decode)
        9    0.001    0.000    0.001    0.000 asn1.py:321(_decodeFromStream)
       29    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        1    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        4    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        1    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        1    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       11    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        1    0.000    0.000    0.001    0.001 asn1.py:480(decode)
        1    0.000    0.000    0.001    0.001 asn1.py:509(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
        9    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       20    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
       72    0.000    0.000    0.000    0.000 asn1.py:61(read)
       38    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
       19    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
      768    0.001    0.000    0.001    0.000 number.py:37(ceil_div)
     6912    0.037    0.000    0.055    0.000 number.py:377(long_to_bytes)
      271    0.021    0.000    0.027    0.000 number.py:419(bytes_to_long)
      256    0.280    0.001    0.280    0.001 number.py:50(size)
      512    0.016    0.000    0.155    0.000 pss.py:163(MGF1)
      371    0.001    0.000    0.001    0.000 py3compat.py:108(bchr)
    20922    0.002    0.000    0.002    0.000 py3compat.py:115(bord)
     9834    0.009    0.000    0.015    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
     8970    0.003    0.000    0.004    0.000 py3compat.py:128(byte_string)
    37539    0.008    0.000    0.012    0.000 py3compat.py:137(is_native_int)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:232(compile)
        2    0.000    0.000    0.000    0.000 re.py:271(_compile)
        1    0.001    0.001    3.340    3.340 rsa.py:90(rsa_decrypt)
      768    0.003    0.000    0.007    0.000 strxor.py:47(strxor)
     6629    0.002    0.000    0.002    0.000 {built-in method _abc._abc_instancecheck}
    25211    0.006    0.000    0.006    0.000 {built-in method _ctypes.byref}
    38912    0.006    0.000    0.006    0.000 {built-in method _struct.pack}
    32783    0.007    0.000    0.007    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
      768    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    3.340    3.340 {built-in method builtins.exec}
  283/282    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
   151581    0.018    0.000    0.022    0.000 {built-in method builtins.isinstance}
    31972    0.003    0.000    0.003    0.000 {built-in method builtins.len}
     1030    0.001    0.000    2.534    0.002 {built-in method builtins.pow}
       12    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
      742    0.003    0.000    0.003    0.000 {built-in method posix.urandom}
      265    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
     9834    0.003    0.000    0.003    0.000 {method 'encode' of 'str' objects}
      256    0.000    0.000    0.000    0.000 {method 'find' of 'bytes' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
     2153    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}


