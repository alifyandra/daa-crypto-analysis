500kb RSA Encrypt Memory Data:
Filename: rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   101     20.9 MiB     20.9 MiB           1   @profile
   102                                         def encrypt_profile(message):
   103     23.5 MiB      2.6 MiB           1       return rsa_encrypt(message)




500kb RSA Decrypt Memory Data:
Filename: rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   105     23.5 MiB     23.5 MiB           1   @profile
   106                                         def decrypt_profile(message):
   107     24.5 MiB      1.1 MiB           1       return rsa_decrypt(message)




500kb RSA Encryption cProfile Data:
         1676839 function calls (1676836 primitive calls) in 3.377 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1009(_handle_fromlist)
        1    0.000    0.000    3.377    3.377 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
     2560    0.003    0.000    0.840    0.000 PKCS1_OAEP.py:71(<lambda>)
     1280    0.018    0.000    3.344    0.003 PKCS1_OAEP.py:88(encrypt)
     1280    0.002    0.000    0.048    0.000 RSA.py:107(n)
     1280    0.006    0.000    0.423    0.000 RSA.py:147(_encrypt)
        1    0.000    0.000    0.000    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.000    0.000 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.000    0.000 RSA.py:618(_import_pkcs1_private)
        2    0.000    0.000    0.001    0.000 RSA.py:638(_import_pkcs1_public)
        1    0.000    0.000    0.001    0.001 RSA.py:647(_import_subjectPublicKeyInfo)
        1    0.000    0.000    0.001    0.001 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.001    0.001 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
    34560    0.021    0.000    0.156    0.000 SHA1.py:142(new)
    34560    0.052    0.000    0.388    0.000 SHA1.py:148(new)
    69120    0.154    0.000    0.283    0.000 SHA1.py:72(__init__)
    33280    0.073    0.000    0.115    0.000 SHA1.py:83(update)
    34560    0.077    0.000    0.140    0.000 SHA1.py:97(digest)
     3848    0.002    0.000    0.003    0.000 _IntegerGMP.py:115(new_mpz)
     3848    0.125    0.000    0.139    0.000 _IntegerGMP.py:150(__init__)
     2560    0.087    0.000    0.097    0.000 _IntegerGMP.py:172(__int__)
     1283    0.003    0.000    0.076    0.000 _IntegerGMP.py:257(_apply_and_return)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:267(__ne__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:275(__le__)
     1280    0.002    0.000    0.080    0.000 _IntegerGMP.py:278(__gt__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:281(__ge__)
     1281    0.001    0.000    0.001    0.000 _IntegerGMP.py:284(__nonzero__)
     2560    0.002    0.000    0.002    0.000 _IntegerGMP.py:288(is_negative)
     1280    0.206    0.000    0.210    0.000 _IntegerGMP.py:355(inplace_pow)
     1280    0.002    0.000    0.218    0.000 _IntegerGMP.py:394(__pow__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
     3846    0.006    0.000    0.006    0.000 _IntegerGMP.py:699(__del__)
        1    0.000    0.000    0.000    0.000 __init__.py:25(_expand_subject_public_key_info)
    39680    0.044    0.000    0.058    0.000 __init__.py:47(create_string_buffer)
     2560    0.004    0.000    0.004    0.000 _raw_api.py:182(get_c_string)
    37120    0.010    0.000    0.010    0.000 _raw_api.py:185(get_raw_buffer)
    40960    0.019    0.000    0.041    0.000 _raw_api.py:219(c_uint8_ptr)
    69120    0.026    0.000    0.026    0.000 _raw_api.py:239(__init__)
    69120    0.012    0.000    0.012    0.000 _raw_api.py:242(get)
    69120    0.025    0.000    0.041    0.000 _raw_api.py:245(address_of)
    69120    0.030    0.000    0.056    0.000 _raw_api.py:248(VoidPointer)
    69120    0.020    0.000    0.020    0.000 _raw_api.py:258(__init__)
    67840    0.011    0.000    0.011    0.000 _raw_api.py:262(get)
    69120    0.063    0.000    0.063    0.000 _raw_api.py:269(__del__)
     1284    0.001    0.000    0.002    0.000 abc.py:137(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       20    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/8    0.000    0.000    0.001    0.000 asn1.py:199(decode)
       20    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        2    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        2    0.000    0.000    0.001    0.000 asn1.py:308(decode)
        2    0.000    0.000    0.001    0.000 asn1.py:321(_decodeFromStream)
       24    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        5    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        7    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:426(__len__)
        2    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        2    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        2    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       16    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        5    0.000    0.000    0.001    0.000 asn1.py:480(decode)
        5    0.000    0.000    0.001    0.000 asn1.py:509(_decodeFromStream)
       10    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
       10    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       33    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
        1    0.000    0.000    0.000    0.000 asn1.py:595(__init__)
       79    0.000    0.000    0.000    0.000 asn1.py:61(read)
        1    0.000    0.000    0.000    0.000 asn1.py:631(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:666(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:682(_decodeFromStream)
        1    0.000    0.000    0.000    0.000 asn1.py:690(<listcomp>)
       49    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
        1    0.000    0.000    0.000    0.000 asn1.py:731(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:760(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:775(_decodeFromStream)
       20    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
     3840    0.004    0.000    0.006    0.000 number.py:37(ceil_div)
    34560    0.186    0.000    0.230    0.000 number.py:377(long_to_bytes)
     1290    0.111    0.000    0.146    0.000 number.py:419(bytes_to_long)
     1280    1.678    0.001    1.678    0.001 number.py:50(size)
     2560    0.086    0.000    0.835    0.000 pss.py:163(MGF1)
      578    0.000    0.000    0.000    0.000 py3compat.py:115(bord)
     2568    0.004    0.000    0.007    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
    40970    0.015    0.000    0.022    0.000 py3compat.py:128(byte_string)
     5130    0.002    0.000    0.003    0.000 py3compat.py:137(is_native_int)
     1281    0.001    0.000    0.002    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:232(compile)
        2    0.000    0.000    0.000    0.000 re.py:271(_compile)
        1    0.032    0.032    3.377    3.377 rsa.py:79(rsa_encrypt)
     2560    0.012    0.000    0.026    0.000 strxor.py:47(strxor)
     1284    0.001    0.000    0.001    0.000 {built-in method _abc._abc_instancecheck}
    72968    0.017    0.000    0.017    0.000 {built-in method _ctypes.byref}
   194560    0.032    0.000    0.032    0.000 {built-in method _struct.pack}
   163850    0.036    0.000    0.036    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
     3841    0.001    0.000    0.001    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    3.377    3.377 {built-in method builtins.exec}
       24    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
   145984    0.026    0.000    0.027    0.000 {built-in method builtins.isinstance}
151185/151184    0.018    0.000    0.018    0.000 {built-in method builtins.len}
     1280    0.001    0.000    0.219    0.000 {built-in method builtins.pow}
        4    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
     1280    0.004    0.000    0.004    0.000 {built-in method posix.urandom}
     1295    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
     2568    0.002    0.000    0.002    0.000 {method 'encode' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        2    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}




500kb RSA Decryption cProfile Data:
         2527453 function calls (2527443 primitive calls) in 18.598 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1009(_handle_fromlist)
        1    0.000    0.000   18.598   18.598 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
     1280    0.059    0.000   18.333    0.014 PKCS1_OAEP.py:142(decrypt)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
     2560    0.005    0.000    0.856    0.000 PKCS1_OAEP.py:71(<lambda>)
        2    0.028    0.014    0.215    0.107 Primality.py:119(lucas_test)
        4    0.000    0.000    0.000    0.000 Primality.py:144(alternate)
        2    0.000    0.000    0.238    0.119 Primality.py:222(test_probable_prime)
       20    0.000    0.000    0.000    0.000 Primality.py:267(<lambda>)
        2    0.000    0.000    0.023    0.011 Primality.py:45(miller_rabin_test)
     1280    0.002    0.000    0.054    0.000 RSA.py:107(n)
        1    0.000    0.000    0.000    0.000 RSA.py:133(u)
     1280    0.036    0.000   15.195    0.012 RSA.py:152(_decrypt)
     1282    0.001    0.000    0.002    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.239    0.239 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.241    0.241 RSA.py:618(_import_pkcs1_private)
        1    0.000    0.000    0.241    0.241 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.242    0.242 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
    34560    0.022    0.000    0.157    0.000 SHA1.py:142(new)
    34560    0.055    0.000    0.403    0.000 SHA1.py:148(new)
    69120    0.159    0.000    0.292    0.000 SHA1.py:72(__init__)
    33280    0.074    0.000    0.118    0.000 SHA1.py:83(update)
    34560    0.079    0.000    0.143    0.000 SHA1.py:97(digest)
     1794    0.012    0.000    0.057    0.000 _IntegerBase.py:297(random)
     1284    0.023    0.000    0.212    0.000 _IntegerBase.py:345(random_range)
    39835    0.020    0.000    0.032    0.000 _IntegerGMP.py:115(new_mpz)
    39835    0.321    0.000    0.445    0.000 _IntegerGMP.py:150(__init__)
     2564    0.097    0.000    0.113    0.000 _IntegerGMP.py:172(__int__)
     1794    0.012    0.000    0.025    0.000 _IntegerGMP.py:234(from_bytes)
    11598    0.018    0.000    0.219    0.000 _IntegerGMP.py:257(_apply_and_return)
     1335    0.002    0.000    0.005    0.000 _IntegerGMP.py:262(__eq__)
     1285    0.004    0.000    0.082    0.000 _IntegerGMP.py:267(__ne__)
     5382    0.007    0.000    0.063    0.000 _IntegerGMP.py:272(__lt__)
     1803    0.001    0.000    0.004    0.000 _IntegerGMP.py:275(__le__)
     1280    0.003    0.000    0.084    0.000 _IntegerGMP.py:278(__gt__)
     1801    0.003    0.000    0.021    0.000 _IntegerGMP.py:281(__ge__)
     5129    0.006    0.000    0.006    0.000 _IntegerGMP.py:284(__nonzero__)
    10252    0.008    0.000    0.008    0.000 _IntegerGMP.py:288(is_negative)
     2566    0.007    0.000    0.034    0.000 _IntegerGMP.py:292(__add__)
     3860    0.014    0.000    0.076    0.000 _IntegerGMP.py:304(__sub__)
     5124    0.023    0.000    0.062    0.000 _IntegerGMP.py:316(__mul__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:328(__floordiv__)
     3844    0.033    0.000    0.059    0.000 _IntegerGMP.py:340(__mod__)
     5128   14.378    0.003   14.396    0.003 _IntegerGMP.py:355(inplace_pow)
     5128    0.008    0.000   14.429    0.003 _IntegerGMP.py:394(__pow__)
     6140    0.007    0.000    0.010    0.000 _IntegerGMP.py:421(__iadd__)
    12282    0.025    0.000    0.031    0.000 _IntegerGMP.py:457(__imul__)
    12294    0.038    0.000    0.039    0.000 _IntegerGMP.py:476(__imod__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
     8204    0.012    0.000    0.012    0.000 _IntegerGMP.py:523(__irshift__)
     4094    0.007    0.000    0.055    0.000 _IntegerGMP.py:553(get_bit)
     8200    0.006    0.000    0.006    0.000 _IntegerGMP.py:567(is_odd)
       12    0.000    0.000    0.000    0.000 _IntegerGMP.py:570(is_even)
     1288    0.003    0.000    0.018    0.000 _IntegerGMP.py:573(size_in_bits)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:584(is_perfect_square)
     6147    0.013    0.000    0.016    0.000 _IntegerGMP.py:601(multiply_accumulate)
    16376    0.016    0.000    0.018    0.000 _IntegerGMP.py:623(set)
     1281    0.118    0.000    0.118    0.000 _IntegerGMP.py:632(inplace_inverse)
     1281    0.003    0.000    0.126    0.000 _IntegerGMP.py:656(inverse)
        3    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:686(jacobi_symbol)
    39827    0.044    0.000    0.044    0.000 _IntegerGMP.py:699(__del__)
     3588    0.002    0.000    0.016    0.000 __init__.py:29(read)
     1282    0.001    0.000    0.001    0.000 __init__.py:46(new)
    40964    0.052    0.000    0.065    0.000 __init__.py:47(create_string_buffer)
     2564    0.005    0.000    0.005    0.000 _raw_api.py:182(get_c_string)
    38400    0.011    0.000    0.011    0.000 _raw_api.py:185(get_raw_buffer)
    44800    0.022    0.000    0.048    0.000 _raw_api.py:219(c_uint8_ptr)
    69120    0.027    0.000    0.027    0.000 _raw_api.py:239(__init__)
    69120    0.012    0.000    0.012    0.000 _raw_api.py:242(get)
    69120    0.025    0.000    0.041    0.000 _raw_api.py:245(address_of)
    69120    0.032    0.000    0.059    0.000 _raw_api.py:248(VoidPointer)
    69120    0.021    0.000    0.021    0.000 _raw_api.py:258(__init__)
    67840    0.012    0.000    0.012    0.000 _raw_api.py:262(get)
    69120    0.064    0.000    0.064    0.000 _raw_api.py:269(__del__)
    16244    0.006    0.000    0.014    0.000 abc.py:137(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       19    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/1    0.000    0.000    0.003    0.003 asn1.py:199(decode)
       19    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        9    0.000    0.000    0.002    0.000 asn1.py:308(decode)
        9    0.002    0.000    0.002    0.000 asn1.py:321(_decodeFromStream)
       29    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        1    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        4    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        1    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        1    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       11    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        1    0.000    0.000    0.003    0.003 asn1.py:480(decode)
        1    0.000    0.000    0.003    0.003 asn1.py:509(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
        9    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       20    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
       72    0.000    0.000    0.000    0.000 asn1.py:61(read)
       38    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
       19    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
     3840    0.005    0.000    0.007    0.000 number.py:37(ceil_div)
    34560    0.225    0.000    0.331    0.000 number.py:377(long_to_bytes)
     1295    0.122    0.000    0.158    0.000 number.py:419(bytes_to_long)
     1280    1.682    0.001    1.682    0.001 number.py:50(size)
     2560    0.089    0.000    0.849    0.000 pss.py:163(MGF1)
     1794    0.003    0.000    0.003    0.000 py3compat.py:108(bchr)
    95049    0.009    0.000    0.009    0.000 py3compat.py:115(bord)
    32136    0.038    0.000    0.065    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
    44810    0.017    0.000    0.024    0.000 py3compat.py:128(byte_string)
    72133    0.024    0.000    0.034    0.000 py3compat.py:137(is_native_int)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:232(compile)
        2    0.000    0.000    0.000    0.000 re.py:271(_compile)
        1    0.021    0.021   18.598   18.598 rsa.py:90(rsa_decrypt)
     3840    0.018    0.000    0.042    0.000 strxor.py:47(strxor)
    16244    0.008    0.000    0.008    0.000 {built-in method _abc._abc_instancecheck}
   108955    0.027    0.000    0.027    0.000 {built-in method _ctypes.byref}
   194560    0.034    0.000    0.034    0.000 {built-in method _struct.pack}
   163855    0.036    0.000    0.036    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
     3840    0.001    0.000    0.001    0.000 {built-in method builtins.divmod}
        1    0.000    0.000   18.598   18.598 {built-in method builtins.exec}
1307/1306    0.001    0.000    0.001    0.000 {built-in method builtins.hasattr}
   417509    0.073    0.000    0.087    0.000 {built-in method builtins.isinstance}
   159347    0.019    0.000    0.019    0.000 {built-in method builtins.len}
     5128    0.007    0.000   14.436    0.003 {built-in method builtins.pow}
       12    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
     3588    0.014    0.000    0.014    0.000 {built-in method posix.urandom}
     1289    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    32136    0.014    0.000    0.014    0.000 {method 'encode' of 'str' objects}
     1280    0.001    0.000    0.001    0.000 {method 'find' of 'bytes' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
    10518    0.003    0.000    0.003    0.000 {method 'pop' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}


