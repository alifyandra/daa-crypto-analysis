1mb 3DES Encrypt Memory Data:
Filename: 3des.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    30     20.7 MiB     20.7 MiB           1   @profile
    31                                         def encrypt_profile(message):
    32     22.6 MiB      1.9 MiB           1       return des3_encrypt(message)




1mb 3DES Decrypt Memory Data:
Filename: 3des.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    34     22.6 MiB     22.6 MiB           1   @profile
    35                                         def decrypt_profile(message):
    36     23.6 MiB      1.0 MiB           1       return des3_decrypt(message)




1mb 3DES Encryption cProfile Data:
         163 function calls in 0.299 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.298    0.298 3des.py:6(des3_encrypt)
        1    0.000    0.000    0.299    0.299 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 DES3.py:114(new)
        2    0.000    0.000    0.000    0.000 DES3.py:60(adjust_key_parity)
       32    0.000    0.000    0.000    0.000 DES3.py:73(parity_byte)
        2    0.000    0.000    0.000    0.000 DES3.py:82(<listcomp>)
        1    0.000    0.000    0.000    0.000 DES3.py:90(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 __init__.py:47(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.298    0.298    0.298    0.298 _mode_cfb.py:124(encrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.001    0.001    0.001    0.001 _raw_api.py:185(get_raw_buffer)
        3    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
       32    0.000    0.000    0.000    0.000 py3compat.py:108(bchr)
        1    0.000    0.000    0.000    0.000 py3compat.py:110(bstr)
       32    0.000    0.000    0.000    0.000 py3compat.py:115(bord)
        3    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.299    0.299 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        8    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        2    0.000    0.000    0.000    0.000 {built-in method posix.urandom}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        2    0.000    0.000    0.000    0.000 {method 'join' of 'bytes' objects}
        5    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}




1mb 3DES Decryption cProfile Data:
         163 function calls in 0.298 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.297    0.297 3des.py:18(des3_decrypt)
        1    0.000    0.000    0.298    0.298 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 DES3.py:114(new)
        2    0.000    0.000    0.000    0.000 DES3.py:60(adjust_key_parity)
       32    0.000    0.000    0.000    0.000 DES3.py:73(parity_byte)
        2    0.000    0.000    0.000    0.000 DES3.py:82(<listcomp>)
        1    0.000    0.000    0.000    0.000 DES3.py:90(_create_base_cipher)
        1    0.000    0.000    0.000    0.000 __init__.py:47(create_string_buffer)
        1    0.000    0.000    0.000    0.000 __init__.py:54(_create_cipher)
        1    0.296    0.296    0.297    0.297 _mode_cfb.py:185(decrypt)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:247(_create_cfb_cipher)
        1    0.000    0.000    0.000    0.000 _mode_cfb.py:74(__init__)
        1    0.001    0.001    0.001    0.001 _raw_api.py:185(get_raw_buffer)
        3    0.000    0.000    0.000    0.000 _raw_api.py:219(c_uint8_ptr)
        2    0.000    0.000    0.000    0.000 _raw_api.py:239(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:242(get)
        2    0.000    0.000    0.000    0.000 _raw_api.py:245(address_of)
        2    0.000    0.000    0.000    0.000 _raw_api.py:248(VoidPointer)
        2    0.000    0.000    0.000    0.000 _raw_api.py:258(__init__)
        2    0.000    0.000    0.000    0.000 _raw_api.py:262(get)
        1    0.000    0.000    0.000    0.000 _raw_api.py:265(release)
        1    0.000    0.000    0.000    0.000 _raw_api.py:269(__del__)
       32    0.000    0.000    0.000    0.000 py3compat.py:108(bchr)
        1    0.000    0.000    0.000    0.000 py3compat.py:110(bstr)
       32    0.000    0.000    0.000    0.000 py3compat.py:115(bord)
        3    0.000    0.000    0.000    0.000 py3compat.py:128(byte_string)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 {built-in method _ctypes.byref}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    0.298    0.298 {built-in method builtins.exec}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        8    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        2    0.000    0.000    0.000    0.000 {built-in method posix.urandom}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        2    0.000    0.000    0.000    0.000 {method 'join' of 'bytes' objects}
        5    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}


