1mb RSA Encrypt Memory Data:
Filename: rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   101     21.6 MiB     21.6 MiB           1   @profile
   102                                         def encrypt_profile(message):
   103     26.1 MiB      4.6 MiB           1       return rsa_encrypt(message)




1mb RSA Decrypt Memory Data:
Filename: rsa.py

Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
   105     26.1 MiB     26.1 MiB           1   @profile
   106                                         def decrypt_profile(message):
   107     27.9 MiB      1.7 MiB           1       return rsa_decrypt(message)




1mb RSA Encryption cProfile Data:
         3433526 function calls (3433523 primitive calls) in 3.974 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1009(_handle_fromlist)
        1    0.000    0.000    3.974    3.974 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
     5244    0.005    0.000    1.081    0.000 PKCS1_OAEP.py:71(<lambda>)
     2622    0.024    0.000    3.842    0.001 PKCS1_OAEP.py:88(encrypt)
     2622    0.002    0.000    0.060    0.000 RSA.py:107(n)
     2622    0.009    0.000    0.555    0.000 RSA.py:147(_encrypt)
        1    0.000    0.000    0.000    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.000    0.000 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.000    0.000 RSA.py:618(_import_pkcs1_private)
        2    0.000    0.000    0.001    0.000 RSA.py:638(_import_pkcs1_public)
        1    0.000    0.000    0.001    0.001 RSA.py:647(_import_subjectPublicKeyInfo)
        1    0.000    0.000    0.001    0.001 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.001    0.001 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
    70794    0.028    0.000    0.197    0.000 SHA1.py:142(new)
    70794    0.067    0.000    0.497    0.000 SHA1.py:148(new)
   141588    0.195    0.000    0.364    0.000 SHA1.py:72(__init__)
    68172    0.092    0.000    0.151    0.000 SHA1.py:83(update)
    70794    0.097    0.000    0.177    0.000 SHA1.py:97(digest)
     7874    0.003    0.000    0.004    0.000 _IntegerGMP.py:115(new_mpz)
     7874    0.191    0.000    0.209    0.000 _IntegerGMP.py:150(__init__)
     5244    0.109    0.000    0.122    0.000 _IntegerGMP.py:172(__int__)
     2625    0.004    0.000    0.113    0.000 _IntegerGMP.py:257(_apply_and_return)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:267(__ne__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:275(__le__)
     2622    0.003    0.000    0.119    0.000 _IntegerGMP.py:278(__gt__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:281(__ge__)
     2623    0.002    0.000    0.002    0.000 _IntegerGMP.py:284(__nonzero__)
     5244    0.002    0.000    0.002    0.000 _IntegerGMP.py:288(is_negative)
     2622    0.247    0.000    0.252    0.000 _IntegerGMP.py:355(inplace_pow)
     2622    0.002    0.000    0.262    0.000 _IntegerGMP.py:394(__pow__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
     7872    0.007    0.000    0.007    0.000 _IntegerGMP.py:699(__del__)
        1    0.000    0.000    0.000    0.000 __init__.py:25(_expand_subject_public_key_info)
    81282    0.055    0.000    0.072    0.000 __init__.py:47(create_string_buffer)
     5244    0.005    0.000    0.005    0.000 _raw_api.py:182(get_c_string)
    76038    0.013    0.000    0.013    0.000 _raw_api.py:185(get_raw_buffer)
    83904    0.025    0.000    0.057    0.000 _raw_api.py:219(c_uint8_ptr)
   141588    0.033    0.000    0.033    0.000 _raw_api.py:239(__init__)
   141588    0.016    0.000    0.016    0.000 _raw_api.py:242(get)
   141588    0.034    0.000    0.054    0.000 _raw_api.py:245(address_of)
   141588    0.040    0.000    0.073    0.000 _raw_api.py:248(VoidPointer)
   141588    0.025    0.000    0.025    0.000 _raw_api.py:258(__init__)
   138966    0.016    0.000    0.016    0.000 _raw_api.py:262(get)
   141588    0.072    0.000    0.072    0.000 _raw_api.py:269(__del__)
     2626    0.001    0.000    0.002    0.000 abc.py:137(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       20    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/8    0.000    0.000    0.001    0.000 asn1.py:199(decode)
       20    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        2    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        2    0.000    0.000    0.000    0.000 asn1.py:308(decode)
        2    0.000    0.000    0.000    0.000 asn1.py:321(_decodeFromStream)
       24    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        5    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        7    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:426(__len__)
        2    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        2    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        2    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       16    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        5    0.000    0.000    0.001    0.000 asn1.py:480(decode)
        5    0.000    0.000    0.001    0.000 asn1.py:509(_decodeFromStream)
       10    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
       10    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       33    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
        1    0.000    0.000    0.000    0.000 asn1.py:595(__init__)
       79    0.000    0.000    0.000    0.000 asn1.py:61(read)
        1    0.000    0.000    0.000    0.000 asn1.py:631(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:666(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:682(_decodeFromStream)
        1    0.000    0.000    0.000    0.000 asn1.py:690(<listcomp>)
       49    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
        1    0.000    0.000    0.000    0.000 asn1.py:731(__init__)
        1    0.000    0.000    0.000    0.000 asn1.py:760(decode)
        1    0.000    0.000    0.000    0.000 asn1.py:775(_decodeFromStream)
       20    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
     7866    0.006    0.000    0.008    0.000 number.py:37(ceil_div)
    70794    0.227    0.000    0.282    0.000 number.py:377(long_to_bytes)
     2632    0.132    0.000    0.175    0.000 number.py:419(bytes_to_long)
     2622    1.717    0.001    1.717    0.001 number.py:50(size)
     5244    0.114    0.000    1.074    0.000 pss.py:163(MGF1)
      578    0.000    0.000    0.000    0.000 py3compat.py:115(bord)
     5252    0.005    0.000    0.010    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
    83914    0.022    0.000    0.031    0.000 py3compat.py:128(byte_string)
    10498    0.003    0.000    0.004    0.000 py3compat.py:137(is_native_int)
     2623    0.002    0.000    0.003    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:232(compile)
        2    0.000    0.000    0.000    0.000 re.py:271(_compile)
        1    0.131    0.131    3.974    3.974 rsa.py:79(rsa_encrypt)
     5244    0.016    0.000    0.035    0.000 strxor.py:47(strxor)
     2626    0.002    0.000    0.002    0.000 {built-in method _abc._abc_instancecheck}
   149462    0.022    0.000    0.022    0.000 {built-in method _ctypes.byref}
   398544    0.039    0.000    0.039    0.000 {built-in method _struct.pack}
   335626    0.043    0.000    0.043    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
     7867    0.002    0.000    0.002    0.000 {built-in method builtins.divmod}
        1    0.000    0.000    3.974    3.974 {built-in method builtins.exec}
       24    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
   298972    0.033    0.000    0.035    0.000 {built-in method builtins.isinstance}
309550/309549    0.024    0.000    0.024    0.000 {built-in method builtins.len}
     2622    0.002    0.000    0.264    0.000 {built-in method builtins.pow}
        4    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
     2622    0.005    0.000    0.005    0.000 {built-in method posix.urandom}
     2637    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
     5252    0.003    0.000    0.003    0.000 {method 'encode' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        2    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}




1mb RSA Decryption cProfile Data:
         4933159 function calls (4933149 primitive calls) in 20.449 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <frozen importlib._bootstrap>:1009(_handle_fromlist)
        1    0.000    0.000   20.449   20.449 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 PEM.py:106(decode)
     2622    0.067    0.000   20.227    0.008 PKCS1_OAEP.py:142(decrypt)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:204(new)
        1    0.000    0.000    0.000    0.000 PKCS1_OAEP.py:36(__init__)
     5244    0.005    0.000    1.122    0.000 PKCS1_OAEP.py:71(<lambda>)
        2    0.018    0.009    0.126    0.063 Primality.py:119(lucas_test)
        4    0.000    0.000    0.000    0.000 Primality.py:144(alternate)
        2    0.000    0.000    0.139    0.069 Primality.py:222(test_probable_prime)
       20    0.000    0.000    0.000    0.000 Primality.py:267(<lambda>)
        2    0.000    0.000    0.012    0.006 Primality.py:45(miller_rabin_test)
     2622    0.003    0.000    0.067    0.000 RSA.py:107(n)
        1    0.000    0.000    0.000    0.000 RSA.py:133(u)
     2622    0.040    0.000   16.700    0.006 RSA.py:152(_decrypt)
     2624    0.001    0.000    0.002    0.000 RSA.py:176(has_private)
        1    0.000    0.000    0.139    0.139 RSA.py:475(construct)
        1    0.000    0.000    0.000    0.000 RSA.py:515(InputComps)
        1    0.000    0.000    0.141    0.141 RSA.py:618(_import_pkcs1_private)
        1    0.000    0.000    0.141    0.141 RSA.py:670(_import_keyDER)
        1    0.000    0.000    0.141    0.141 RSA.py:712(import_key)
        1    0.000    0.000    0.000    0.000 RSA.py:77(__init__)
    70794    0.030    0.000    0.204    0.000 SHA1.py:142(new)
    70794    0.072    0.000    0.518    0.000 SHA1.py:148(new)
   141588    0.201    0.000    0.377    0.000 SHA1.py:72(__init__)
    68172    0.095    0.000    0.156    0.000 SHA1.py:83(update)
    70794    0.101    0.000    0.184    0.000 SHA1.py:97(digest)
     3669    0.014    0.000    0.075    0.000 _IntegerBase.py:297(random)
     2626    0.027    0.000    0.270    0.000 _IntegerBase.py:345(random_range)
    77134    0.025    0.000    0.038    0.000 _IntegerGMP.py:115(new_mpz)
    77134    0.429    0.000    0.581    0.000 _IntegerGMP.py:150(__init__)
     5248    0.120    0.000    0.137    0.000 _IntegerGMP.py:172(__int__)
     3669    0.015    0.000    0.031    0.000 _IntegerGMP.py:234(from_bytes)
    19373    0.019    0.000    0.282    0.000 _IntegerGMP.py:257(_apply_and_return)
     2676    0.002    0.000    0.005    0.000 _IntegerGMP.py:262(__eq__)
     2627    0.004    0.000    0.114    0.000 _IntegerGMP.py:267(__ne__)
     6724    0.005    0.000    0.048    0.000 _IntegerGMP.py:272(__lt__)
     3678    0.002    0.000    0.004    0.000 _IntegerGMP.py:275(__le__)
     2622    0.003    0.000    0.121    0.000 _IntegerGMP.py:278(__gt__)
     3676    0.003    0.000    0.027    0.000 _IntegerGMP.py:281(__ge__)
    10496    0.007    0.000    0.007    0.000 _IntegerGMP.py:284(__nonzero__)
    20987    0.010    0.000    0.010    0.000 _IntegerGMP.py:288(is_negative)
     5250    0.009    0.000    0.042    0.000 _IntegerGMP.py:292(__add__)
     7886    0.016    0.000    0.091    0.000 _IntegerGMP.py:304(__sub__)
    10492    0.027    0.000    0.072    0.000 _IntegerGMP.py:316(__mul__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:328(__floordiv__)
     7870    0.038    0.000    0.070    0.000 _IntegerGMP.py:340(__mod__)
    10495   15.624    0.001   15.645    0.001 _IntegerGMP.py:355(inplace_pow)
    10495    0.010    0.000   15.685    0.001 _IntegerGMP.py:394(__pow__)
     6140    0.004    0.000    0.006    0.000 _IntegerGMP.py:421(__iadd__)
    12282    0.014    0.000    0.018    0.000 _IntegerGMP.py:457(__imul__)
    12294    0.021    0.000    0.022    0.000 _IntegerGMP.py:476(__imod__)
        1    0.000    0.000    0.000    0.000 _IntegerGMP.py:491(__and__)
     8204    0.007    0.000    0.007    0.000 _IntegerGMP.py:523(__irshift__)
     4094    0.004    0.000    0.033    0.000 _IntegerGMP.py:553(get_bit)
     8200    0.003    0.000    0.003    0.000 _IntegerGMP.py:567(is_odd)
       12    0.000    0.000    0.000    0.000 _IntegerGMP.py:570(is_even)
     2630    0.003    0.000    0.023    0.000 _IntegerGMP.py:573(size_in_bits)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:584(is_perfect_square)
     6147    0.007    0.000    0.009    0.000 _IntegerGMP.py:601(multiply_accumulate)
    16376    0.009    0.000    0.010    0.000 _IntegerGMP.py:623(set)
     2623    0.163    0.000    0.164    0.000 _IntegerGMP.py:632(inplace_inverse)
     2623    0.003    0.000    0.173    0.000 _IntegerGMP.py:656(inverse)
        3    0.000    0.000    0.000    0.000 _IntegerGMP.py:661(gcd)
        2    0.000    0.000    0.000    0.000 _IntegerGMP.py:686(jacobi_symbol)
    77126    0.048    0.000    0.048    0.000 _IntegerGMP.py:699(__del__)
     7338    0.002    0.000    0.023    0.000 __init__.py:29(read)
     2624    0.001    0.000    0.001    0.000 __init__.py:46(new)
    83908    0.064    0.000    0.081    0.000 __init__.py:47(create_string_buffer)
     5248    0.006    0.000    0.006    0.000 _raw_api.py:182(get_c_string)
    78660    0.015    0.000    0.015    0.000 _raw_api.py:185(get_raw_buffer)
    91770    0.030    0.000    0.066    0.000 _raw_api.py:219(c_uint8_ptr)
   141588    0.033    0.000    0.033    0.000 _raw_api.py:239(__init__)
   141588    0.016    0.000    0.016    0.000 _raw_api.py:242(get)
   141588    0.036    0.000    0.056    0.000 _raw_api.py:245(address_of)
   141588    0.043    0.000    0.076    0.000 _raw_api.py:248(VoidPointer)
   141588    0.027    0.000    0.027    0.000 _raw_api.py:258(__init__)
   138966    0.016    0.000    0.016    0.000 _raw_api.py:262(get)
   141588    0.073    0.000    0.073    0.000 _raw_api.py:269(__del__)
    28855    0.007    0.000    0.015    0.000 abc.py:137(__instancecheck__)
       10    0.000    0.000    0.000    0.000 asn1.py:143(_convertTag)
       19    0.000    0.000    0.000    0.000 asn1.py:184(_decodeLen)
     10/1    0.000    0.000    0.001    0.001 asn1.py:199(decode)
       19    0.000    0.000    0.000    0.000 asn1.py:222(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:277(__init__)
        9    0.000    0.000    0.001    0.000 asn1.py:308(decode)
        9    0.001    0.000    0.001    0.000 asn1.py:321(_decodeFromStream)
       29    0.000    0.000    0.000    0.000 asn1.py:33(_is_number)
        1    0.000    0.000    0.000    0.000 asn1.py:387(__init__)
        4    0.000    0.000    0.000    0.000 asn1.py:411(__getitem__)
        1    0.000    0.000    0.000    0.000 asn1.py:437(hasInts)
        1    0.000    0.000    0.000    0.000 asn1.py:446(<listcomp>)
        1    0.000    0.000    0.000    0.000 asn1.py:449(hasOnlyInts)
       11    0.000    0.000    0.000    0.000 asn1.py:46(__init__)
        1    0.000    0.000    0.001    0.001 asn1.py:480(decode)
        1    0.000    0.000    0.001    0.001 asn1.py:509(_decodeFromStream)
        9    0.000    0.000    0.000    0.000 asn1.py:51(set_bookmark)
        9    0.000    0.000    0.000    0.000 asn1.py:54(data_since_bookmark)
       20    0.000    0.000    0.000    0.000 asn1.py:58(remaining_data)
       72    0.000    0.000    0.000    0.000 asn1.py:61(read)
       38    0.000    0.000    0.000    0.000 asn1.py:70(read_byte)
       19    0.000    0.000    0.000    0.000 asn1.py:80(__init__)
     7866    0.007    0.000    0.009    0.000 number.py:37(ceil_div)
    70794    0.257    0.000    0.388    0.000 number.py:377(long_to_bytes)
     2637    0.141    0.000    0.186    0.000 number.py:419(bytes_to_long)
     2622    1.715    0.001    1.715    0.001 number.py:50(size)
     5244    0.121    0.000    1.114    0.000 pss.py:163(MGF1)
     3669    0.004    0.000    0.004    0.000 py3compat.py:108(bchr)
   192430    0.012    0.000    0.012    0.000 py3compat.py:115(bord)
    61384    0.046    0.000    0.078    0.000 py3compat.py:117(tobytes)
        1    0.000    0.000    0.000    0.000 py3compat.py:126(tostr)
    91780    0.025    0.000    0.034    0.000 py3compat.py:128(byte_string)
   117483    0.026    0.000    0.036    0.000 py3compat.py:137(is_native_int)
        1    0.000    0.000    0.000    0.000 py3compat.py:148(_copy_bytes)
        2    0.000    0.000    0.000    0.000 re.py:232(compile)
        2    0.000    0.000    0.000    0.000 re.py:271(_compile)
        1    0.078    0.078   20.449   20.449 rsa.py:90(rsa_decrypt)
     7866    0.025    0.000    0.055    0.000 strxor.py:47(strxor)
    28855    0.008    0.000    0.008    0.000 {built-in method _abc._abc_instancecheck}
   218722    0.034    0.000    0.034    0.000 {built-in method _ctypes.byref}
   398544    0.042    0.000    0.042    0.000 {built-in method _struct.pack}
   335631    0.045    0.000    0.045    0.000 {built-in method _struct.unpack}
        1    0.000    0.000    0.000    0.000 {built-in method binascii.a2b_base64}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__build_class__}
     7866    0.002    0.000    0.002    0.000 {built-in method builtins.divmod}
        1    0.000    0.000   20.449   20.449 {built-in method builtins.exec}
2649/2648    0.001    0.000    0.001    0.000 {built-in method builtins.hasattr}
   766108    0.082    0.000    0.098    0.000 {built-in method builtins.isinstance}
   326288    0.026    0.000    0.026    0.000 {built-in method builtins.len}
    10495    0.008    0.000   15.693    0.001 {built-in method builtins.pow}
       12    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
     7338    0.021    0.000    0.021    0.000 {built-in method posix.urandom}
     2631    0.001    0.000    0.001    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'decode' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    61384    0.017    0.000    0.017    0.000 {method 'encode' of 'str' objects}
     2622    0.002    0.000    0.002    0.000 {method 'find' of 'bytes' objects}
        2    0.000    0.000    0.000    0.000 {method 'group' of 're.Match' objects}
        1    0.000    0.000    0.000    0.000 {method 'items' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'keys' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'match' of 're.Pattern' objects}
    21511    0.004    0.000    0.004    0.000 {method 'pop' of 'dict' objects}
        1    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
        1    0.000    0.000    0.000    0.000 {method 'search' of 're.Pattern' objects}
        1    0.000    0.000    0.000    0.000 {method 'split' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'bytes' objects}
        1    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}


