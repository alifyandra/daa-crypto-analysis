import subprocess
import sys

sizes = ['100k','500k','1m','5m','10m']
i = int(sys.argv[1])
algos = sys.argv[2:]

print(f"Report {i+1}")
subprocess.run(f'mkdir reports-gcp/{i+1}', shell=True)
for algo in algos:
    for size in sizes:
        subprocess.run(f'mkdir reports-gcp/{i+1}/{algo}', shell=True)
        subprocess.run(f'python3 {algo}.py {size} > reports-gcp/{i+1}/{algo}/{algo}_{size}.txt', shell=True)
